---
layout: post
title: Java Software Development Setup
date: 2020-11-21 17:32
category:
- Software Development
- Java
author: markcrowe
keywords: Java, Tools, Utilites, Windows 10
summary: Software Development Tools for Java
thumbnail: /assets/img/posts/hello.jpg
permalink: /blog/java-software-development-setup/
tags:
- Software Development
- Java
summary: 
---
# Java Software Development Setup

For winget version see [Java Software Development Setup Winget](/blog/java-software-development-setup/)

# Software Develeopment Tools

## Git
<div style="text-align: justify ">
Git is a source code management version-control system for tracking changes in any set of files and allows for coordinating work among programmers cooperating on source code during software development.  An added benifit is that when used with an online service such as GitLab or GitHub is provides a remote backup of the files.
</div>
For students the benifits 
1. you can track your changes
2. alway go back to previous working version
3. View old version for use in reports.
4. Have a backup
5. THirdParty proof of work done by a time.
6. Experience for industry.
7. Profilo

[Download Git](https://git-scm.com/downloads)

## GitHub Desktop

[Download GitHub Desktop](https://desktop.github.com/)

## WinMerge

[Download WinMerge](https://winmerge.org/)

# Database Develeopment Tools

## MySQL

[Download MySQL Community Installer](https://dev.mysql.com/downloads/installer/)

## XAMPP

[Download XAMPP](https://www.apachefriends.org/index.html)

# Java Develeopment Tools

## Java JDK

### Java SE Development Kit 8 
[Download Java SE Development Kit 8](https://www.oracle.com/ie/java/technologies/javase/javase-jdk8-downloads.html)


Register with your K number

Details are not given under oat.

## Apache NetBeans
[Download Apache NetBeans](https://netbeans.apache.org/download/)

### Plugins

### Web Servers

#### Apache Tomcat
[Download Apache Tomcat](https://tomcat.apache.org/)

#### GlassFish
[Download GlassFish](https://javaee.github.io/glassfish/)
